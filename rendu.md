**instructions**

Lisez les instructions ici [droits_unix.pdf](droits_unix.pdf), et
compilez votre [rendu.md](rendu.md).


# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome 
- Dhaussy Etienne, Etienne.Dhaussy.etu@univ-lille.fr
- Merceret Fabien, Fabien.Merceret.etu@univ-lille.fr 

## Question 1 

Oui, les permissions de `titi.txt` sont les suivantes :
- toto est en lecture seule
- les membres du groupe ubuntu peuvent lire et écrire
- n'importe quel autre utilisateur est en lecture seule

toto étant memebre du groupe ubuntu, le processus qu'il a lancé a les permissions pour écrire.

## Question 2 

- Pour un répertoire, x signifie qu'il peut être consulté par l'utilisateur.

- Toto ne peut plus rentrer dans le dossier. L'utilisateur toto n'a plus accès au dossier mydir car il est membre du groupe ubuntu.

> d rwx `rw-` r-x toto ubuntu mydir

- Toto ne peut pas connaître les permissions des contenus de mydir car le groupe ubuntu n'a pas le droit d'exuter des processus sur mydir. Il a le droit de savoir qu'il y a des contenus dans mydir, et leurs noms, mais rien de plus.
    - C'est parce que toto a le droit de LIRE les contenus, mais pas d'eXécuter de scripts dans mydir.

## Question 3 

Le contenu du fichier est imprimé quand ubuntu lance suid, et pas quand toto lance suid

"""
Losrque toto lance avant le chmod u+s suid:
Procédé 73547
User ID 1001
Effective user ID 1001        <--- ID de toto
Le programme ne lit pas le fichier data.txt.
"""

"""
Procédé 73582
User ID 1001
Effective user ID 1000        <--- ID de ubuntu
Plop

M A R I O

ptit minecraft ?
"""

Après la commande, l'utilisateur toto arrive à lancer le programme qui réussit à lire le fichier data.txt



questions de permission

## Question 4

toto@isi:/root/ISI/mario$ python3 suid.py
1001
1001

Les droits uid et guid sont ceux de l'utilisateur ubuntu

## Question 5

chfn sert à modifier les informations de l'utilisateur stockées sur le fichier /etc/passwd
les permission de chfn sont -rwsr-xr-x 1 root root 85064, ce qui signifie que n'importe quel utilisateur peut executer la
commande avec les droits root.


## Question 6

Les mots de passe des utilisateur sont stockés dans /etc/shadow
Ils sont chiffrés et le fichier n'est accessible qu'au root.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








