#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

int main(int argc, char *argv[])
{
    printf("Procédé %d\n", getpid());
    printf("User ID %d\n", getuid());
    printf("Effective user ID %d\n", geteuid());
    
    int c;
    FILE *file;
    file = fopen("mydir/data.txt", "r");
    if (file) {
    while ((c = getc(file)) != EOF)
        putchar(c);
    fclose(file);
    }

    return 0;
}
